﻿using System;
using System.IO;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using SepaKON.Models;
using System.Diagnostics;
using System.Xml.Schema;
using Microsoft.Ajax.Utilities;

namespace SepaKON.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(Document document)
        {
            try
            {
                return this.Content(loadXML(document), "text/xml");
            }
            catch (Exception ex)
            {
                return this.Content("<error>" + ex.Message + "</error>", "text/xml");
            }
        }

        private string loadXML(Document document)
        {
            //Creating own namespace overriding .NET default ones
            XmlSerializerNamespaces nameSpaces = new XmlSerializerNamespaces();

            //Adding own namespace string
            nameSpaces.Add("", "urn:iso:std:iso:20022:tech:xsd:scthr:pain.001.001.03");

            //Creating serializer object
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Document));

            //Serialize the object with our own namespaces
            var stringWriter = new StringWriter();
            var xmlWriter = XmlWriter.Create(stringWriter);
            xmlWriter.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"");
            xmlSerializer.Serialize(xmlWriter, document, nameSpaces);
            var xmlOutput = stringWriter.ToString();
            isXmlValid(xmlOutput);
            return xmlOutput;
        }

        private void isXmlValid(string xmlOutput)
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.Schemas.Add(null, ControllerContext.HttpContext.Server.MapPath("~/XSD/sepa.hr.pain.001.001.03_07052015.xsd"));
            settings.ValidationType = ValidationType.Schema;

            XmlReader reader = XmlReader.Create(new StringReader(xmlOutput), settings);
            XmlDocument document = new XmlDocument();
            document.Load(reader);

            ValidationEventHandler eventHandler = new ValidationEventHandler(ValidationEventHandler);

            document.Validate(eventHandler);
        }

        static void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            switch (e.Severity)
            {
                case XmlSeverityType.Error:
                    Console.WriteLine("Error: {0}", e.Message);
                    throw new Exception("Validacija Sheme nije uspješna");
                    break;
                case XmlSeverityType.Warning:
                    Console.WriteLine("Warning {0}", e.Message);
                    throw new Exception("Validacija Sheme nije uspješna");
                    break;
            }
        }
    }
}
