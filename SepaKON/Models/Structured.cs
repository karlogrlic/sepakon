﻿using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (Structured) Strukturirani
    /// </summary>
    /// <remarks>Neobavezan podatak.</remarks>
    [XmlRoot]
    public class Strd
    {
        /// <summary>
        /// (Creditor Reference Information) Referenca koju određuje primatelj plaćanja
        /// </summary>
        /// <remarks>Neobavezan podatak.</remarks>
        [XmlElement]
        public CdtrRefInf CdtrRefInf { get; set; }

        /// <summary>
        /// (Additional Remittance Information) Dodatni detalji plaćanja
        /// </summary>
        /// <remarks>Neobavezan podatak.</remarks>
        [XmlElement]
        public string AddtlRmtInf { get; set; }
    }
}