﻿using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (Scheme Name) Naziv Scheme
    /// </summary>
    /// <remarks>Neobavezan podatak.</remarks>
    [XmlRoot]
    public class SchmeNm
    {
        /// <summary>
        /// (Code) Kod
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public string Cd { get; set; }
    }
}