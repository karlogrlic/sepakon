﻿using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (Credit Transfer Transaction Information) Informacija o transakciji kreditnog transfera.
    /// </summary>
    /// <remarks>Obavezan podatak.</remarks>
    [XmlRoot]
    public class CdtTrfTxInf
    {
        /// <summary>
        /// (Payment Identification) Identifikacijska oznaka plaćanja.
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public PmtId PmtId { get; set; }

        /// <summary>
        /// (Amount) Iznos
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public Amt Amt { get; set; }

        /// <summary>
        /// (Creditor) Primatelj plaćanja.
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public Cdtr Cdtr { get; set; }

        /// <summary>
        /// (Creditor Account) Račun primatelja.
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public CdtrAcct CdtrAcct { get; set; }

        /// <summary>
        /// (Purpose) Šifra namjene
        /// </summary>
        /// <remarks>Neobavezan podatak.</remarks>
        [XmlElement]
        public Purp Purp { get; set; }

        /// <summary>
        /// (Remittance Information) Detalji plaćanja
        /// </summary>
        /// <remarks>Neobavezan podatak.</remarks>
        [XmlElement]
        public RmtInf RmtInf { get; set; }
    }
}