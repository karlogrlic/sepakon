﻿using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (Payment Identification) Identifikacijska oznaka plaćanja.
    /// </summary>
    /// <remarks>Obavezan podatak.</remarks>
    [XmlRoot]
    public class PmtId
    {
        /// <summary>
        /// (Instruction Identification) Identifikacija oznaka naloga-referencija.
        /// </summary>
        /// <remarks>Neobavezan podatak.</remarks>
        [XmlElement]
        public string InstrId { get; set; }

        /// <summary>
        /// (End to End Identification) Jedinstveni identifikator.
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public string EndToEndId { get; set; }
    }
}