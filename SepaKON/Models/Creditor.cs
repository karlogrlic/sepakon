﻿using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (Creditor) Primatelj plaćanja.
    /// </summary>
    /// <remarks>Obavezan podatak.</remarks>
    [XmlRoot]
    public class Cdtr
    {
        /// <summary>
        /// (Name) Naziv primatelja plaćanja.
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public string Nm { get; set; }

        /// <summary>
        /// (Postal Adress) Adresa primatelja.
        /// </summary>
        /// <remarks>Neobavezan podatak.</remarks>
        [XmlElement]
        public PstlAdr PstlAdr { get; set; }

        /// <summary>
        /// (Identification) Identifikacija.
        /// </summary>
        /// <remarks>Neobavezan podatak.</remarks>
        [XmlElement]
        public Id Id { get; set; }
    }
}