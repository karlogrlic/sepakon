using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (Ultimate Debtor) Stvrani du�nik
    /// </summary>
    /// <remarks>Neobavezan podatak.</remarks>
    [XmlRoot]
    public class UltmtDbtr
    {
        /// <summary>
        /// (Identification) Identifikacija
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public Id Id { get; set; }
    }
}