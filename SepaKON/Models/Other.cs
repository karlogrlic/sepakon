using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (Other) Ostalo
    /// </summary>
    /// <remarks>Obavezan podatak.</remarks>
    [XmlRoot]
    public class Othr
    {
        /// <summary>
        /// (Identification) Identifikacija
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public string Id { get; set; }

        /// <summary>
        /// (Scheme Name) Naziv Scheme
        /// </summary>
        /// <remarks>Neobavezan podatak.</remarks>
        [XmlElement]
        public SchmeNm SchmeNm { get; set; }
    }
}