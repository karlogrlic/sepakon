﻿using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (Creditor Reference Information) Referenca koju određuje primatelj plaćanja
    /// </summary>
    /// <remarks>Neobavezan podatak.</remarks>
    [XmlRoot]
    public class CdtrRefInf
    {
        /// <summary>
        /// (Type) Vrsta reference
        /// </summary>
        /// <remarks>Neobavezan podatak.</remarks>
        [XmlElement]
        public Tp Tp { get; set; }

        /// <summary>
        /// (Reference) Referentna oznaka
        /// </summary>
        /// <remarks>Neobavezan podatak.</remarks>
        [XmlElement]
        public string Ref { get; set; }
    }
}