using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (Financial Institution Identification) Oznaka PPU platitelja
    /// </summary>
    /// <remarks>Obavezan podatak.</remarks>
    [XmlRoot]
    public class FinInstnId
    {
        /// <summary>
        /// (Other) Ostalo
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public Othr Othr { get; set; }
    }
}