﻿using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (Payment Type Information) Informacije o vrsti plaćanja
    /// </summary>
    /// <remarks>Neobavezan podatak.</remarks>
    [XmlRoot]
    public class PmtTpInf
    {
        /// <summary>
        /// (Instruction Priority) Oznaka hitnosti
        /// </summary>
        /// <remarks>Neobavezan podatak.</remarks>
        [XmlElement]
        public string InstrPrty { get; set; }
        
        /// <summary>
        /// (CategoryPurpose) Kategorija namjene
        /// </summary>
        /// <remarks>Neobavezan podatak.</remarks>
        [XmlElement]
        public CtgyPurp CtgyPurp { get; set; }
    }
}