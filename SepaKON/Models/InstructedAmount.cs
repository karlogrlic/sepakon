﻿using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (Instructed Amount) Oznaka valute i iznos naloga za plaćanje.
    /// </summary>
    /// <remarks>Obavezan podatak.</remarks>
    [XmlRoot]
    public class InstdAmt
    {
        /// <summary>
        /// (Currency) Valuta transakcijskog računa
        /// </summary>
        /// <remarks>Neobavezan podatak.</remarks>
        [XmlAttribute(AttributeName = "Ccy")]
        public string Ccy { get; set; }

        /// <summary>
        /// Tekst
        /// </summary>
        [XmlText]
        public string Text { get; set; }
    }
}