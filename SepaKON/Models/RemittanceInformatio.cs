﻿using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (Remittance Information) Detalji plaćanja
    /// </summary>
    /// <remarks>Neobavezan podatak.</remarks>
    [XmlRoot]
    public class RmtInf
    {
        /// <summary>
        /// (Structured) Strukturirani
        /// </summary>
        /// <remarks>Neobavezan podatak.</remarks>
        [XmlElement]
        public Strd Strd { get; set; }
    }
}