using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (Debtor Agent) PPU platitelja
    /// </summary>
    /// <remarks>Obavezan podatak.</remarks>
    [XmlRoot]
    public class DbtrAgt
    {
        /// <summary>
        /// (Financial Institution Identification) Oznaka PPU platitelja
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public FinInstnId FinInstnId { get; set; }
    }
}