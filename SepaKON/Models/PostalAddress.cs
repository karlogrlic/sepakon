using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (Postal Address) Adresa platitelja
    /// </summary>
    /// <remarks>Neobavezan podatak.</remarks>
    [XmlRoot]
    public class PstlAdr
    {
        /// <summary>
        /// (Address Line) Adresna linija
        /// </summary>
        /// <remarks>Neobavezan podatak.</remarks>
        [XmlElement]
        public string AdrLine { get; set; }

        /// <summary>
        /// (Country) Dr�ava
        /// </summary>
        /// <remarks>Neobavezan podatak.</remarks>
        [XmlElement]
        public string Ctry { get; set; }
    }
}