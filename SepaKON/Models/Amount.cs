﻿using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary> 
    /// (Amount) Iznos naloga za plaćanje.
    /// </summary>
    /// <remarks>Obavezan podatak.</remarks>
    [XmlRoot]
    public class Amt
    {
        /// <summary>
        /// (InstructedAmount) Oznaka valute i iznos naloga za plaćanje.
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public InstdAmt InstdAmt { get; set; }
    }
}