using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (Identification) Identifikacija
    /// </summary>
    /// <remarks>Obavezan podatak.</remarks>
    [XmlRoot]
    public class Id
    {
        /// <summary>
        /// (Organisation Identification) Oznaka Identifikacije
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public OrgId OrgId { get; set; }
        /// <summary>
        /// IBAN
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public string IBAN { get; set; }
    }
}