﻿using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (Debtor Account) Račun platitelja
    /// </summary>
    [XmlRoot]
    public class DbtrAcct
    {
        /// <summary>
        /// (Identification) Identifikacija
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public Id Id { get; set; }

        /// <summary>
        /// (Currency) Valuta transakcijskog računa
        /// </summary>
        /// <remarks>Neobavezan podatak.</remarks>
        [XmlElement]
        public string Ccy { get; set; }
    }
}