﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (CategoryPurpose) Kategorija namjene
    /// </summary>
    /// <remarks>Neobavezan podatak.</remarks>
    [XmlRoot]
    public class CtgyPurp
    {
        /// <summary>
        /// (Code) Kod
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public string Cd { get; set; }
    }
}