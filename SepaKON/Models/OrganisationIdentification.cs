using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (Organisation Identification) Oznaka Identifikacije
    /// </summary>
    /// <remarks>Obavezan podatak.</remarks>
    [XmlRoot]
    public class OrgId
    {
        /// <summary>
        /// (Other) Ostalo
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public Othr Othr { get; set; }
    }
}