﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (Purpose) Šifra namjene
    /// </summary>
    /// <remarks>Neobavezan podatak.</remarks>
    [XmlRoot]
    public class Purp
    {
        /// <summary>
        /// (Code) Kod
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public string Cd { get; set; }
    }
}