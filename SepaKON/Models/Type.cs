using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (Type) Vrsta reference.
    /// </summary>
    /// <remarks>Neobavezan podatak.</remarks>
    [XmlRoot]
    public class Tp
    {
        /// <summary>
        /// (Code or Proprietary) Kod ili vlastita oznaka.
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public CdOrPrtry CdOrPrtry { get; set; }
    }
}