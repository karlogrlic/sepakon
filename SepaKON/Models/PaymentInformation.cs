﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (Payment Information) Informacija o plaćanju 
    /// </summary>
    /// <remarks>Obavezan podatak.</remarks>
    [XmlRoot]
    public class PmtInf
    {
        /// <summary>
        /// (Payment Information Identification) Informacija o plaćanju.
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public string PmtInfId { get; set; }

        /// <summary>
        /// (Payment Method) Način plaćanja.
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public string PmtMtd { get; set; }

        /// <summary>
        /// (Batch Booking) Način terećenja računa platitelja.
        /// </summary>
        /// <remarks>Neobavezan podatak.</remarks>
        [XmlElement]
        public string BtchBookg { get; set; }

        /// <summary>
        /// (Number of Transactions) Broj naloga.
        /// </summary>
        /// <remarks>Neobavezan podatak.</remarks>
        [XmlElement]
        public string NbOfTxs { get; set; }

        /// <summary>
        /// (Control Sum) Kontrolna suma iznosa.
        /// </summary>
        /// <remarks>Neobavezan podatak.</remarks>
        [XmlElement]
        public string CtrlSum { get; set; }

        /// <summary>
        /// (Payment Type Information) Informacije o vrsti plaćanja
        /// </summary>
        /// <remarks>Neobavezan podatak.</remarks>
        [XmlElement]
        public PmtTpInf PmtTpInf { get; set; }

        /// <summary>
        /// (Requested Execution Date) Traženi datum izvršenja
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public string ReqdExctnDt { get; set; }

        /// <summary>
        /// (Debtor) Platitelj
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public Dbtr Dbtr { get; set; }

        /// <summary>
        /// (Debtor Account) Račun Platitelja
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public DbtrAcct DbtrAcct { get; set; }

        /// <summary>
        /// (Debtor Agent) PPU platitelja
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public DbtrAgt DbtrAgt { get; set; }

        /// <summary>
        /// (Ultimate Debtor) Stvrani dužnik
        /// </summary>
        /// <remarks>Neobavezan podatak.</remarks>
        [XmlElement]
        public UltmtDbtr UltmtDbtr { get; set; }

        /// <summary>
        /// (Charge Bearer) Troškovna opcija
        /// </summary>
        /// <remarks>Neobavezan podatak.</remarks>
        [XmlElement]
        public string ChrgBr { get; set; }

        /// <summary>
        /// (Credit Transfer Transaction Information) Informacija o transakciji kreditnog transfera
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public List<CdtTrfTxInf> CdtTrfTxInf { get; set; }
    }
}