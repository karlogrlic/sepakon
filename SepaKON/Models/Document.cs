﻿using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// Root čvor podataka.
    /// </summary>
    [XmlRoot(Namespace = "urn:iso:std:iso:20022:tech:xsd:scthr:pain.001.001.03")]
    public class Document
    {
        /// <summary>
        /// (Message root) Oznaka poruke – labela
        /// </summary>
        /// <remarks> Obavezan podatak. </remarks>
        [XmlElement]
        public CstmrCdtTrfInitn CstmrCdtTrfInitn { get; set; }
    }
}