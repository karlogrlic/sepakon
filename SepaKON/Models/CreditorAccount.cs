﻿using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (Creditor Account) Račun primatelja.
    /// </summary>
    /// <remarks>Obavezan podatak.</remarks>
    [XmlRoot]
    public class CdtrAcct
    {
        /// <summary>
        /// (Identification) Identifikacija
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public Id Id { get; set; }
    }
}