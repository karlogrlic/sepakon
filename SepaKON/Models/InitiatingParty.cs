﻿using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (Initiating Party) Inicijator plaćanja.
    /// </summary>
    /// <remarks>Obavezan podatak.</remarks>
    [XmlRoot]
    public class InitgPty
    {
        /// <summary>
        /// (Name) Naziv.
        /// </summary>
        /// <remarks>Neobavezan podatak.</remarks>
        [XmlElement]
        public string Nm { get; set; }

        /// <summary>
        /// (Identification) Identifikacija
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public Id Id { get; set; }
    }
}