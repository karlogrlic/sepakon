﻿using System.Runtime.CompilerServices;
using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (Group Header) Zaglavlje ili vodeći slog.
    /// </summary>
    /// <remarks>Obavezan podatak.</remarks>
    [XmlRoot]
    public class GrpHdr
    {
        /// <summary>
        /// (Message Identification) Jedinstveni identifikator poruke u okviru istog datuma nastanka i istog pošiljatelja.
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public string MsgId { get; set; }

        /// <summary>
        /// (Creation Date Time) Datum i vrijeme kreiranja poruke.
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public string CreDtTm { get; set; }

        /// <summary>
        /// (Number Of Transactions) Broj naloga.
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public string NbOfTxs { get; set; }

        /// <summary>
        /// (Control Sum) Kontrolni zbroj iznosa. 
        /// </summary>
        /// <remarks>Neobavezan podatak.</remarks>
        [XmlElement]
        public string CtrlSum { get; set; }

        /// <summary>
        /// Initiating Party (Inicijator plaćanja) 
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public InitgPty InitgPty { get; set; }
    }
}