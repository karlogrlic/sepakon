using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (Debtor) Platitelj
    /// </summary>
    /// <remarks>Obavezan podatak.</remarks>
    [XmlRoot]
    public class Dbtr
    {
        /// <summary>
        /// (Name) Naziv platitelja
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public string Nm { get; set; }

        /// <summary>
        /// (Postal Address) Adresa platitelja
        /// </summary>
        /// <remarks>Neobavezan podatak.</remarks>
        [XmlElement]
        public PstlAdr PstlAdr { get; set; }

        /// <summary>
        /// (Identification) Identifikacija
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public Id Id { get; set; }
    }
}