using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (Code or Proprietary) Kod ili vlastita oznaka.
    /// </summary>
    /// <remarks>Obavezan podatak.</remarks>
    [XmlRoot]
    public class CdOrPrtry
    {
        /// <summary>
        /// (Code) Kod.
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public string Cd { get; set; }
    }
}