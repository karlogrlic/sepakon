﻿using System.Xml.Serialization;

namespace SepaKON.Models
{
    /// <summary>
    /// (Message root) Oznaka poruke – labela
    /// </summary>
    /// <remarks>Obavezan podatak.</remarks>
    [XmlRoot]
    public class CstmrCdtTrfInitn
    {
        /// <summary>
        /// (Group Header) Zaglavlje ili vodeći slog
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public GrpHdr GrpHdr { get; set; }

        /// <summary>
        /// (Payment Information) Informacija o plaćanju 
        /// </summary>
        /// <remarks>Obavezan podatak.</remarks>
        [XmlElement]
        public PmtInf PmtInf { get; set; }
    }
}